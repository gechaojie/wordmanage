#!/usr/bin/env python
# coding=utf-8
"""for accessing database. """

import traceback
import torndb

RAWNUM = 20

class EZConnection(torndb.Connection):
    def __init__(self, db_host, db_name, db_user, db_pswd):
        super(EZConnection, self).__init__(
            host=db_host,
            database=db_name,
            user=db_user,
            password=db_pswd
        )
        self.log = None
        self.execute('set time_zone = "SYSTEM"')

    def set_log(self, func):
        self.log = func

    def is_in_table(self, table_name, field, value):
        if isinstance(value, unicode):
            value = value.encode('utf8')
        sql = ('SELECT %s FROM %s '
               'WHERE %s="%s"') % (field, table_name, field, value)
        d = self.get(sql)
        if d:
            return True
        return False

    def execute(self, query, *parameters):
        try:
            lastrowid = self.execute_lastrowid(query, *parameters)
        except Exception, e:
            if e[0] == 1062:
                # "Duplicate entry"
                lastrowid = 0
                pass
            else:
                traceback.print_exc()
                print 'sql:', query
                print 'VALUES:', parameters
                raise e
        if self.log:
            lower_query = query.lower()
            if (lower_query.startswith('update') or
                lower_query.startswith('insert')):
                try:
                    self.log(query, *parameters)
                except:
                    print 'self.log error !!!!!!!!!!!!!!'
                    traceback.print_exc()
        return lastrowid

    def item_to_table(self, table_name, item):
        '''item if a dict : key is mysql table field'''
        fields = item.keys()
        values = item.values()
        fieldstr = ','.join(fields)
        valstr = ','.join(['%s'] * len(item))
        for i in xrange(len(values)):
            if isinstance(values[i], unicode):
                values[i] = values[i].encode('utf8')
        sql = 'INSERT INTO %s (%s) VALUES(%s)' % (table_name, fieldstr, valstr)
        try:
            last_id = self.execute(sql, *values)
            return last_id
        except Exception, e:
            # traceback.print_exc()
            if e[0] == 1062: # just skip duplicated item
                pass
            else:
                traceback.print_exc()
                print 'sql:', sql
                print 'values:', values
                print 'item:'
                for i in xrange(len(fields)):
                    print fields[i], ' : ', values[i], type(values[i])
                raise e

    def update_table(self, table_name, updates,
                     field_where, value_where):
        '''updates is a dict of {field_update:value_update}'''
        upsets = []
        values = []
        for k, v in updates.items():
            s = '%s=%%s' % k
            upsets.append(s)
            values.append(v)
        upsets = ','.join(upsets)
        sql = 'UPDATE %s SET %s WHERE %s="%s"' % (
            table_name,
            upsets,
            field_where, value_where,
        )
        self.execute(sql, *(values))

    def update_dirty(self, id):
        self.execute("UPDATE words SET pos = '0', type = '0', good = '0' WHERE id = %s" % id)

    def update_pt(self, id, pos, type):
        self.execute("UPDATE words SET pos = '%s',type = '%s', good = '1' WHERE id = %s" % (pos, type, id))

    def get_info(self, page, good):
        result = self.query('select * from words where good=%s order by id desc' % good)
        return result[page*RAWNUM:page*RAWNUM+RAWNUM]

    def page_count(self, good):
        count = self.query('select count(id) from words where good=%s' % good)[0]['count(id)']
        count = int(count)
        return {'num':count/RAWNUM, 'is_over':count%RAWNUM}


if __name__ == '__main__':
    pass
