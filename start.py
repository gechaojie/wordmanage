# coding=utf-8

import os.path
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from db_instance import db


from tornado.options import define, options
define("port", default=8000, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
                (r'/', IndexHandler),
                (r'/poem', PoemHandler),
                (r'/hello', PageDisplay),
                ]
        settings = dict(
                template_path=os.path.join(os.path.dirname(__file__), "templates"),
                # static_path=os.path.join(os.dirname(__file__), "static"),
                # ui_modules={'Hello':HelloModule}
                # debug=True
                )
        tornado.web.Application.__init__(self, handlers, **settings)

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        info = db.get_info(0, 2)
        count = db.page_count(2)
        self.render("hello.html", info=info, page=0, count=count, good=2)

    def post(self):
        good = int(self.get_argument('good'))
        info = db.get_info(0, good)
        count = db.page_count(good)
        print count
        self.render("hello.html", info=info, page=0, count=count, good=good)

class PoemHandler(tornado.web.RequestHandler):
    def post(self):
        page = self.get_argument('page')
        page = int(page)
        id = self.get_argument('id')
        isdelete = int(self.get_argument('isdelete'))
        if int(self.get_argument('isdelete')) == 0:
            pos = self.get_argument('pos')
            type_ = self.get_argument('type')
            if int(type_)==0:
                db.update_dirty(id)
            else:
                db.update_pt(id, pos, type_)
        else:
            db.update_dirty(id)

        good = int(self.get_argument('good'))
        info = db.get_info(page, good)
        count = db.page_count(good)
        # value = self.get_body_argument('value')
        self.render("hello.html", info=info, page=page, count=count, good=good)
        # self.render("poem.html", roads=pos, wood=type_, made=page, difference='4')


class PageDisplay(tornado.web.RequestHandler):
    def post(self):
        page = int(self.get_argument('page')) - 1
        good = int(self.get_argument('good'))

        info = db.get_info(page, good)
        count = db.page_count(good)
        # value = self.get_body_argument('value')
        self.render("hello.html", info=info, page=page, count=count, good=good)
        # self.render("poem.html", roads=pos, wood=type_, made=page, difference='4')

if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


